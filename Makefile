include .env

UNAME=$(shell uname -s)

DOCKER_FILES=-f docker-compose.yml

ifeq (${UNAME},Linux)
	DOCKER_FILES += -f docker-compose-linux.yml
endif

ifeq ($(UNAME),Darwin)
	DOCKER_FILES += -f docker-compose-macos.yml
endif

default: help

.PHONY: help
help:  ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

################################################################################
## Setup.
################################################################################

.PHONY: init
init: ## Init local files.
	@echo "$(COLOR_LIGHT_GREEN)Init local files...$(COLOR_NC)"
	rsync -avz --ignore-existing ./example.env ./.env

.PHONY: install
install: ## Install mkcert.
	@echo "$(COLOR_LIGHT_GREEN)Installing mkcert...$(COLOR_NC)"
	./install-mkcert.sh

	@echo "$(COLOR_LIGHT_GREEN)Generating local certificates...$(COLOR_NC)"
	./generate-local-certificate.sh

	@echo "$(COLOR_LIGHT_GREEN)Creating docker network...$(COLOR_NC)"
	docker network create traefik

################################################################################
## Docker.
################################################################################

.PHONY: docker-up
docker-up: ## Start containers.
	@echo "$(COLOR_LIGHT_GREEN)Starting up containers...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) ${DOCKER_FILES} up

.PHONY: docker-upd
docker-upd: ## Start containers in detached mode and with auto-restart.
	@echo "$(COLOR_LIGHT_GREEN)Starting up containers...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) ${DOCKER_FILES} -f docker-compose-auto-restart.yml up -d

.PHONY: docker-stop
docker-stop: ## Stop containers.
	@echo "$(COLOR_LIGHT_GREEN)Stopping containers...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) stop

.PHONY: docker-down
docker-down: ## Remove containers.
	@echo "$(COLOR_LIGHT_GREEN)Removing containers...$(COLOR_NC)"
	@$(DOCKER_COMPOSE_COMMAND) down

# https://stackoverflow.com/a/6273809/1826109
%:
	@:
