# Docker Traefik

Traefik configuration used for local development.

Works for Linux (with APT) and Mac OS.

## Installation

After cloning the project, execute the following commands:

```shell script
make init
make install
```

This will install [mkcert](https://github.com/FiloSottile/mkcert) on your computer
and generate a self-signed certificate for `*.docker.localhost` domains.

Also mkcert will install a local CA authority that will recognize the previously
generated self-signed certificate, and allow it on your system and your browsers.
So you will have to manually accept it.

It will then create a docker network named `traefik`.

## Usage

After the installation and on a daily basis, just execute the following command:

```shell script
make docker-up
```

This will give you a Traefik using the certificates.

### Autostart

You can also use a daemon version with `restart: always`

And execute the command:

```shell script
make docker-upd
```

This way Traefik will restart automatically when you start your computer and no
more need of having a terminal opened with Traefik running.

To stop it execute the command:

```shell script
make docker-stop
```
