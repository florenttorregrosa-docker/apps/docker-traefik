#!/usr/bin/env bash

set -o allexport
source .env
set +o allexport

echo -e "${LIGHT_GREEN}Generate certificate for '*.docker.localhost'.${NC}"
mkcert -cert-file ./certs/_wildcard.docker.localhost.pem -key-file ./certs/_wildcard.docker.localhost-key.pem "*.docker.localhost"
